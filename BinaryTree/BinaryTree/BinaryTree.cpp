// BinaryTree.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>

using namespace std;

template<class DataType>
class Element
{
public:
    DataType data;
    Element* parent;
    Element* lower;
    Element* higher;
    Element()
    {
        data = NULL;
        parent = nullptr;
        lower = nullptr;
        higher = nullptr;
    }
	Element(DataType input)
	{
		data = input;
		parent = nullptr;
		lower = nullptr;
		higher = nullptr;
	}
};

//Binary Tree. Sorted. Doesn't accept duplicates. lowest to highest.
template<class DataType>
class BinaryTree
{
public:
    Element<DataType>* root;
    Element<DataType>* operator[](DataType target)
    {
		Element<DataType>* returnMe = Search(target);
		if (returnMe->data == target)
		{
			return returnMe;
		}
		else
		{
			return nullptr;
		}
	}
	bool Contains(DataType& target)
	{
		Element<DataType>* returnMe = Search(target);
		if (returnMe->data == target)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void insert(DataType& input)
	{
		data.push_back(Element<DataType>(input));
		rebuild();
	}
	void insert(DataType* a, int size)
	{
		for (size_t i = 0; i < size; i++)
		{
			data.push_back(Element<DataType>(a[i]));
		}
		rebuild();
	}
private:
	vector<Element<DataType>> data;
	Element<DataType>* Search(DataType& target)
	{
		Element<DataType>* returnMe = root;
		while (true)
		{
			if (returnMe->data == target)
			{
				return returnMe;
			}
			else if (returnMe->lower == nullptr && returnMe->higher == nullptr)
			{
				return returnMe;
			}
			else if (target < returnMe->data && returnMe->lower != nullptr)
			{
				returnMe = returnMe->lower;
				continue;
			}
			else if (target < returnMe->data && returnMe->lower == nullptr)
			{
				return returnMe;
			}
			else if (target > returnMe->data && returnMe->higher != nullptr)
			{
				returnMe = returnMe->higher;
				continue;
			}
			else if (target > returnMe->data && returnMe->higher == nullptr)
			{
				return returnMe;
			}
			else
			{
				std::cout << "Tree search loop encountered an error." << std::endl;
				return nullptr;
			}
		}
	}
	void sort()
	{
		QuickSort(data, 0, data.size() - 1);
	}
	void removeDupes()
	{
		for (size_t i = 0; i < data.size - 1; )
		{
			if (data[i].data == data[i + 1].data)
			{
				data.erase(data.begin() + i);
			}
			else
				i++;
		}
	}
	void rebuild()
	{		
		sort();
		removeDupes();
		root = &data[findMeanValue(0, data.size())];
		treeIt(findMeanValue(0, data.size()), 0, data.size());
	}
	void treeIt(int index, int lowIndex, int highIndex)
	{
		int temp = findMeanValue(lowIndex, index - 1);
		if (temp == -1)
		{
			data[index].lower = nullptr;
		}
		else
		{
			data[index].lower = &data[temp];
			treeIt(temp, lowIndex, index - 1);
		}
		temp = findMeanValue(index + 1, highIndex);
		if (temp == -1)
		{
			data[index].higher = nullptr;
		}
		else
		{
			data[index].higher = &data[temp];
			treeIt(temp, index + 1, highIndex);
		}
	}
	int findMeanValue(int lowIndex, int highIndex)
	{
		if (lowIndex >= highIndex)
		{
			return -1;
		}
		else if (lowIndex == highIndex)
		{
			return lowIndex;
		}
		else
		{
			return (lowIndex + highIndex) / 2;
		}
	}

#pragma region QuickSort

	//this function helps 3-way swap on the left side of the pivot
	void LeftSwap(vector<Element<DataType>> array, int pivotIndex, int highIndex)
	{
		if (highIndex == pivotIndex - 1)
		{
			//just 2-way swap them
			DataType temp1 = array[pivotIndex - 1].data;
			array[pivotIndex - 1] = array[pivotIndex];
			array[pivotIndex].data = temp1;
		}
		else
		{
			//pivot goes one to the left, high number goes to pivot, 'one to the left' goes to high number
			DataType ottl = array[pivotIndex - 1].data;
			DataType pivotValue = array[pivotIndex].data;
			DataType highValue = array[highIndex].data;
			array[pivotIndex - 1].data = pivotValue;
			array[pivotIndex].data = highValue;
			array[highIndex].data = ottl;
		}
	}

	//this function helps 3-way swap on the right side of the pivot
	void rightSwap(vector<Element<DataType>> array, int pivotIndex, int lowIndex)
	{
		if (lowIndex == pivotIndex + 1)
		{
			//just 2-way swap them
			DataType temp1 = array[pivotIndex + 1].data;
			array[pivotIndex + 1].data = array[pivotIndex].data;
			array[pivotIndex].data = temp1;
		}
		else
		{
			//pivot goes one to the right, low number goes to pivot, 'one to the right' goes to low number
			DataType ottr = array[pivotIndex + 1].data;
			DataType pivotValue = array[pivotIndex].data;
			DataType lowValue = array[lowIndex].data;
			array[pivotIndex + 1].data = pivotValue;
			array[pivotIndex].data = lowValue;
			array[lowIndex].data = ottr;
		}
	}

	//i'm using the 'middle' as a pivot.
	int Partition(vector<Element<DataType>> array, int lowIndex, int highIndex)
	{
		//std::cout << "Sorting from " << lowIndex << " to " << highIndex << ". ";
		int span = highIndex - lowIndex + 1;
		int pivotIndex = lowIndex + span / 2;
		int pivotValue = array[pivotIndex].data;
		//std::cout << "Pivot value is : " << pivotValue << std::endl;
		for (int i = lowIndex; i <= highIndex; )
		{
			//don't evaluate the pivot
			if (i == pivotIndex)
			{
				i++;
			}
			//left side of the pivot
			else if (i < pivotIndex)
			{
				//numbers larger than our pivot must be shifted right
				if (array[i].data <= pivotValue)
				{
					i++;
				}
				else
				{
					LeftSwap(array, pivotIndex, i);
					pivotIndex--;
				}
			}
			//right side of the pivot
			else // i > pivotIndex
			{
				//numbers smaller than our pivot must be shifted left
				if (array[i].data >= pivotValue)
				{
					i++;
				}
				else
				{
					rightSwap(array, pivotIndex, i);
					pivotIndex++;
				}
			}
		}
		//PrintArray(array, 25);
		return pivotIndex;
	}

	//recursively quicksort and int array
	void QuickSort(vector<Element<DataType>> array, int lowIndex, int highIndex)
	{
		if (lowIndex < highIndex)
		{
			int pivot = Partition(array, lowIndex, highIndex);
			if (pivot >= lowIndex)
				QuickSort(array, lowIndex, pivot - 1);
			if (pivot <= highIndex)
				QuickSort(array, pivot, highIndex);
		}
	}

#pragma endregion
};

int main()
{
    std::cout << "Hello World!\n";
    system("pause");
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
