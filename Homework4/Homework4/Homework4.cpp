// Homework4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

//print the whole array
void PrintArray(const int* a, const size_t& size)
{
	for (size_t i = 0; i < size; i++)
	{
		std::cout << a[i] << " ";
	}
	std::cout << std::endl;
}

void InsertionSort(int* a, const size_t& size)
{
	//track the highest value so far
	int highestValue = a[0];
	for (size_t i = 0; i < size; i++)
	{
		//if we found a new highest value it stays at the end
		if (a[i] >= highestValue)
		{
			highestValue = a[i];
		}
		else
		{
			//if we found a different value, we work backwards from the end, bumping everything forward until we find where it goes
			//then we paste it in
			int cacheValue = a[i];
			for (size_t j = 0; i >= j; j++)
			{
				size_t currentPosition = i - j;
				size_t previousPosition = currentPosition - 1;
				//this case is if: we are at position 0. just paste it in 0.
				if (currentPosition == 0)
				{
					a[currentPosition] = cacheValue;
					break;
				}
				else if (a[previousPosition] >= cacheValue)
				{
					a[currentPosition] = a[previousPosition];
					continue;
				}
				else
				{
					a[previousPosition] = cacheValue;
					break;
				}
			}
		}
	}
}

void MergeSort(int* inputArray, const size_t& inputSize)
{
	//if  less than 2 items, nothing
	//if just 2, simple sort
	if (inputSize == 2)
	{
		if (inputArray[0] > inputArray[1])
		{
			int cache = inputArray[0];
			inputArray[0] = inputArray[1];
			inputArray[1] = cache;
		}
	}
	//Currently overwrites the input data as it works. WRONG.
	else if (inputSize > 2)
	{
		//split the array into first half (A) and second half(B)
		int* A = inputArray;
		int* B = &inputArray[inputSize / 2];
		//recursively call MergeSort on those halves.
		MergeSort(A, inputSize / 2);
		MergeSort(B, inputSize - (inputSize / 2));
		//iterator i for A, iterator j for B
		size_t i = 0;
		size_t iLength = inputSize / 2;
		size_t j = 0;
		size_t jLength = inputSize - iLength;
		//sort into a different array first;
		int* outputArray = new int[inputSize];
		for (size_t pos = 0; pos < inputSize; pos++)
		{
			if (i < iLength && j < jLength)
			{
				if (A[i] < B[j])
				{
					outputArray[pos] = A[i];
					i++;
				}
				else
				{
					outputArray[pos] = B[j];
					j++;
				}
			}
			else if (i < iLength)
			{
				outputArray[pos] = A[i];
				i++;
			}
			else
			{
				outputArray[pos] = B[j];
				j++;
			}
		}
		//copy the sorted list into the target location
		for (size_t pos = 0; pos < inputSize; pos++)
		{
			inputArray[pos] = outputArray[pos];
		}
		delete[] outputArray;
	}
}

int main()
{
	std::cout << "Hello World!\n";
	//make an array of random numbers (0-99), 50 elements long
	size_t testArraySize = 50;
	int* testArray = new int[testArraySize];
	for (size_t i = 0; i < testArraySize; i++)
	{
		testArray[i] = rand() % 100;
	}
	PrintArray(testArray, testArraySize);
	system("pause");
	//InsertionSort(testArray, testArraySize);
	MergeSort(testArray, testArraySize);
	PrintArray(testArray, testArraySize);
	system("pause");

	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
