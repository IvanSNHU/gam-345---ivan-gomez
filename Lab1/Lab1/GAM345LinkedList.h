#pragma once
namespace GAM345
{
	template <class DataType>
	class LinkedListElement
	{
	private:
		//private stuff
	public:
		//LinkedListElement<DataType>* previous;
		LinkedListElement<DataType>* next;
		DataType data;

		LinkedListElement<DataType>() : next(nullptr) {}
		LinkedListElement<DataType>(DataType newData) : next(nullptr), data(newData) {}
		~LinkedListElement<DataType>()
		{
			//previous = nullptr;
			next = nullptr;
		}
	};

	template <class DataType>
	class LinkedList
	{
	private:
		LinkedListElement<DataType>* first;
		size_t count;
		void insert(LinkedListElement<DataType>* element, size_t position)
		{
			if (first == nullptr)
			{
				first = element;
			}
			else if (position == 0)
			{
				element->next = first;
				//first->previous = &element;
				first = element;
			}
			else if (position == count)
			{
				//element.previous = &operator[](position - 1);
				operator[](position - 1).next = element;
			}
			else
			{
				//element.previous = &operator[](position - 1);
				element->next = &operator[](position);
				operator[](position - 1).next = element;
				//element.previous->next = &element;
				//element.next->previous = &element;
			}
			count++;
			//operator[](position)
		}
	public:
		LinkedList<DataType>() : first(nullptr), count(0)
		{
			//
		}
		~LinkedList<DataType>()
		{
			clear();
		}
		void insert(DataType newData, size_t position)
		{
			insert(new LinkedListElement<DataType>(newData), position);
		}
		void erase(size_t position)
		{
#pragma region old
			//if (operator[](position).next == nullptr && operator[](position).previous == nullptr)
			//{
			//	delete first;
			//	first = nullptr;
			//}
			//else if (operator[](position).next != nullptr && operator[](position).previous != nullptr)
			//{				
			//	LinkedListElement<DataType>* temp = &operator[](position);
			//	temp->next->previous = temp->previous;
			//	temp->previous->next = temp->next;
			//	delete temp;
			//}
			//else if (operator[](position).next == nullptr && operator[](position).previous != nullptr)
			//{
			//	LinkedListElement<DataType>* temp = &operator[](position);
			//	temp->previous->next = nullptr;
			//	delete temp;
			//}
			//else if (operator[](position).next != nullptr && operator[](position).previous == nullptr)
			//{
			//	LinkedListElement<DataType>* temp = &operator[](position);
			//	temp->next->previous = nullptr;
			//	first = temp->next;
			//	delete temp;
			//}
			//else
			//{
			//	std::cout << "erase case 5: this should never happen" << std::endl;
			//	//should never happen
			//}
			//std::cout << "count: " << count << std::endl;
#pragma endregion
			//
			if (position == 0)
			{
				if (first == nullptr)
				{
					return;
				}
				else
				{
					if (first->next != nullptr)
					{
						LinkedListElement<DataType>* temp = first->next;
						delete first;
						first = temp;
					}
					else
					{
						delete first;
						first = nullptr;
					}
				}
			}
			else if (position == count - 1)
			{
				LinkedListElement<DataType>* temp = &operator[](position);
				operator[](position - 1).next = nullptr;
				delete temp;
			}
			else
			{
				LinkedListElement<DataType>* temp = &operator[](position);
				operator[](position - 1).next = temp->next;
				delete temp;
			}
			count--;
		}
		LinkedListElement<DataType>& operator[](size_t position)
		{
			LinkedListElement<DataType>* returnMe = first;
			for (size_t i = 0; i < position; i++)
			{
				returnMe = (*returnMe).next;
				if (returnMe == nullptr)
				{
					break;
				}
			}
			return *returnMe;
		}
		void clear()
		{
			for (size_t i = 0; i < count; i++)
			{
				erase(0);
			}
		}
		size_t size()
		{
			return count;
		}
	};
}