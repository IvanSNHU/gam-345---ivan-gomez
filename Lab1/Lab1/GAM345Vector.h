#pragma once
namespace GAM345
{
	template <class DataType>
	class Vector
	{
	private:
		DataType* data;
		int currentSize;
		int currentCapacity;
	public:
		//default constructor. size/capacity zero.
		Vector<DataType>() : currentCapacity(0), currentSize(0), data(nullptr)
		{
		}

		//constructor that reserves some capacity space.
		Vector<DataType>(int newSize) : currentCapacity(newSize), currentSize(newSize), data(nullptr)
		{
		}

		//destructor
		~Vector()
		{
			if (data == nullptr)
			{
				return;
			}
			else
			{
				delete[] data;
			}
		}

		//change capacity AND size to a value
		void resize(int newSize)
		{
			DataType* temp = (DataType*)new char[newSize * sizeof(DataType)];
			for (int i = 0; i < newSize; i++)
			{
				if (i < currentSize) { temp[i] = data[i]; }
				else { temp[i] = DataType(); }
			}
			delete[] data;
			data = temp;
			currentCapacity = newSize;
			currentSize = newSize;
		}

		//fetch the size (not the total capacity, just what is being used
		int size()
		{
			return currentSize;
		}

		//set our capacity to be this much higher than the current size
		void reserve(int addedCapacity)
		{
			const int newCapacity = currentSize + addedCapacity;
			DataType* temp = (DataType*)new char[newCapacity * sizeof(DataType)];
			for (int i = 0; i < currentSize; i++)
			{
				temp[i] = data[i];
			}
			delete[] data;
			data = temp;
			currentCapacity = newCapacity;
		}

		//add an element to the end.
		void push_back(DataType data)
		{
			if (currentCapacity > currentSize)
			{
				currentSize++;
				this->data[currentSize - 1] = data;
			}
			else
			{
				reserve(1);
				push_back(data);
			}
		}

		//is it empty of data?
		bool empty()
		{
			if (currentSize > 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		//what is the current max capacity before I need to allocate more memory?
		int capacity()
		{
			return currentCapacity;
		}

		//remove this element, and squish down everything else
		void remove(int index)
		{
			for (int i = index; i < currentSize - 1; i++)
			{
				data[i] = data[i + 1];
			}
			currentSize--;
		}

		//add this element, and push back everything else
		void insert(DataType data, int index)
		{
			reserve(1);
			for (int i = currentSize - 1; i > index + 1; i--)
			{
				this->data[i] = this->data[i - 1];
			}
			this->data[index] = data;
		}

		//clear all the data. size/capacity are zero
		void clear()
		{
			currentCapacity = 0;
			currentSize = 0;
			delete[] data;
			data = nullptr;
		}

		//subscript operator.
		DataType& operator[](int index)
		{
			return data[index];
		}
	};
}