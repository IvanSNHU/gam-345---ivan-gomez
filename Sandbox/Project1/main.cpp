#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <Windows.h>

using namespace std;

int main(int argc, char** argv)
{
	const size_t length = 10000000;

#pragma region Vector
	auto startTime = GetTickCount64();

	cout << "Hello World!" << endl;
	vector<int> ints;
	for (size_t i = 0; i < length; i++)
	{
		ints.push_back(i);
	}

	auto endTime = GetTickCount64();
	cout << (endTime - startTime) << endl;
#pragma endregion

#pragma region List
	startTime = GetTickCount64();

	list<int> listInts;
	for (size_t i = 0; i < length; i++)
	{
		listInts.push_back(i);
	}

	endTime = GetTickCount64();
	cout << (endTime - startTime) << endl;
#pragma endregion

	return 0;
}