//this function helps 3-way swap on the left side of the pivot
void LeftSwap(int array[], int pivotIndex, int highIndex)
{
	if (highIndex == pivotIndex - 1)
	{
		//just 2-way swap them
		int temp1 = array[pivotIndex - 1];
		array[pivotIndex - 1] = array[pivotIndex];
		array[pivotIndex] = temp1;
	}
	else
	{
		//pivot goes one to the left, high number goes to pivot, 'one to the left' goes to high number
		int ottl = array[pivotIndex - 1];
		int pivotValue = array[pivotIndex];
		int highValue = array[highIndex];
		array[pivotIndex - 1] = pivotValue;
		array[pivotIndex] = highValue;
		array[highIndex] = ottl;
	}
}

//this function helps 3-way swap on the right side of the pivot
void rightSwap(int array[], int pivotIndex, int lowIndex)
{
	if (lowIndex == pivotIndex + 1)
	{
		//just 2-way swap them
		int temp1 = array[pivotIndex + 1];
		array[pivotIndex + 1] = array[pivotIndex];
		array[pivotIndex] = temp1;
	}
	else
	{
		//pivot goes one to the right, low number goes to pivot, 'one to the right' goes to low number
		int ottr = array[pivotIndex + 1];
		int pivotValue = array[pivotIndex];
		int lowValue = array[lowIndex];
		array[pivotIndex + 1] = pivotValue;
		array[pivotIndex] = lowValue;
		array[lowIndex] = ottr;
	}
}

//i'm using the 'middle' as a pivot.
int Partition(int array[], int lowIndex, int highIndex)
{
	std::cout << "Sorting from " << lowIndex << " to " << highIndex << ". ";
	int span = highIndex - lowIndex + 1;
	int pivotIndex = lowIndex + span / 2;
	int pivotValue = array[pivotIndex];
	std::cout << "Pivot value is : " << pivotValue << std::endl;
	for (int i = lowIndex; i <= highIndex; )
	{
		//don't evaluate the pivot
		if (i == pivotIndex)
		{
			i++;
		}
		//left side of the pivot
		else if (i < pivotIndex)
		{
			//numbers larger than our pivot must be shifted right
			if (array[i] <= pivotValue)
			{
				i++;
			}
			else
			{
				LeftSwap(array, pivotIndex, i);
				pivotIndex--;
			}
		}
		//right side of the pivot
		else // i > pivotIndex
		{
			//numbers smaller than our pivot must be shifted left
			if (array[i] >= pivotValue)
			{
				i++;
			}
			else
			{
				rightSwap(array, pivotIndex, i);
				pivotIndex++;
			}
		}
	}
	PrintArray(array, 25);
	return pivotIndex;
}

//recursively quicksort and int array
void QuickSort(int array[], int lowIndex, int highIndex)
{
	if (lowIndex < highIndex)
	{
		int pivot = Partition(array, lowIndex, highIndex);
		if (pivot >= lowIndex)
			QuickSort(array, lowIndex, pivot - 1);
		if (pivot <= highIndex)
			QuickSort(array, pivot, highIndex);
	}
}