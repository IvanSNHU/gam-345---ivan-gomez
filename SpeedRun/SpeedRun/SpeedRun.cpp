// SpeedRun.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

#pragma region QuickSort

//this function helps 3-way swap on the left side of the pivot
void LeftSwap(int* array, int pivotIndex, int highIndex)
{
	if (highIndex == pivotIndex - 1)
	{
		//just 2-way swap them
		int temp1 = array[pivotIndex - 1];
		array[pivotIndex - 1] = array[pivotIndex];
		array[pivotIndex] = temp1;
	}
	else
	{
		//pivot goes one to the left, high number goes to pivot, 'one to the left' goes to high number
		int ottl = array[pivotIndex - 1];
		int pivotValue = array[pivotIndex];
		int highValue = array[highIndex];
		array[pivotIndex - 1] = pivotValue;
		array[pivotIndex] = highValue;
		array[highIndex] = ottl;
	}
}

//this function helps 3-way swap on the right side of the pivot
void rightSwap(int* array, int pivotIndex, int lowIndex)
{
	if (lowIndex == pivotIndex + 1)
	{
		//just 2-way swap them
		int temp1 = array[pivotIndex + 1];
		array[pivotIndex + 1] = array[pivotIndex];
		array[pivotIndex] = temp1;
	}
	else
	{
		//pivot goes one to the right, low number goes to pivot, 'one to the right' goes to low number
		int ottr = array[pivotIndex + 1];
		int pivotValue = array[pivotIndex];
		int lowValue = array[lowIndex];
		array[pivotIndex + 1] = pivotValue;
		array[pivotIndex] = lowValue;
		array[lowIndex] = ottr;
	}
}

//i'm using the 'middle' as a pivot.
int Partition(int* array, int lowIndex, int highIndex)
{
	//std::cout << "Sorting from " << lowIndex << " to " << highIndex << ". ";
	int span = highIndex - lowIndex + 1;
	int pivotIndex = lowIndex + span / 2;
	int pivotValue = array[pivotIndex];
	//std::cout << "Pivot value is : " << pivotValue << std::endl;
	for (int i = lowIndex; i <= highIndex; )
	{
		//don't evaluate the pivot
		if (i == pivotIndex)
		{
			i++;
		}
		//left side of the pivot
		else if (i < pivotIndex)
		{
			//numbers larger than our pivot must be shifted right
			if (array[i] <= pivotValue)
			{
				i++;
			}
			else
			{
				LeftSwap(array, pivotIndex, i);
				pivotIndex--;
			}
		}
		//right side of the pivot
		else // i > pivotIndex
		{
			//numbers smaller than our pivot must be shifted left
			if (array[i] >= pivotValue)
			{
				i++;
			}
			else
			{
				rightSwap(array, pivotIndex, i);
				pivotIndex++;
			}
		}
	}
	//PrintArray(array, 25);
	return pivotIndex;
}

//recursively quicksort and int array
void QuickSort(int* array, int lowIndex, int highIndex)
{
	if (lowIndex < highIndex)
	{
		int pivot = Partition(array, lowIndex, highIndex);
		if (pivot >= lowIndex)
			QuickSort(array, lowIndex, pivot - 1);
		if (pivot <= highIndex)
			QuickSort(array, pivot, highIndex);
	}
}

#pragma endregion


void PrintArray(int* array, int size)
{
	for (size_t i = 0; i < size; i++)
	{
		cout << array[i] << " ";
	}
}

//handle the list of numbers
void ParseMiddle(string* input, int loops, int* output)
{
	stringstream ss(*input);
	for (size_t i = 0; i < loops; i++)
	{
		int temp = 0;
		ss >> temp;
		//output->push_back(temp);
		output[i] = temp;
	}
}

bool FindValueInSortedArray(int value, int* inputArray, int size)
{
	bool returnMe = false;

	float step = size / 4.0f;
	int i = size / 2.0f;
	do
	{
		//cout << endl << inputArray[i] << endl;
		if (inputArray[i] == value)
		{
			//cout << "equal to" << endl;
			return true;
		}
		else if (inputArray[i] > value)
		{
			i -= (step + 0.5f);
			step /= 2.0f;
			//cout << "greater" << endl;
			continue;
		}
		else //inputArray[i] < value
		{
			i += (step + 0.5f);
			step /= 2.0f;
			//cout << "less" << endl;
			continue;
		}
	} while (step > 0.125f);

	return returnMe;
}

bool FindDuplicatesInSortArray(int* inputArray, int size)
{
	for (size_t i = 0; i < size - 1; i++)
	{
		if (inputArray[i] == inputArray[i + 1])
		{
			return true;
		}
	}
	return false;
}

int main()
{
	cout << "Hello World!\n";
	ifstream ifs("Numbers.txt");	
#pragma region First	
	string output = "";
	getline(ifs, output);
	const int loops = stoi(output);
#pragma endregion
#pragma region Middle
	int* holding = new int[loops];
	{
		string middle = "";
		getline(ifs, middle);
		ParseMiddle(&middle, loops, holding);
	}
#pragma endregion
#pragma region Last
	int target = 0;
	{
		string output = "";
		getline(ifs, output);
		target = stoi(output);
	}
#pragma endregion

	cout << "Here's what I read: " << endl;
	cout << "List size: " << loops << endl;
	//PrintArray(holding, loops);
	cout << "Target value: " << target << endl;

	QuickSort(holding, 0, loops - 1);
	//cout << "Here is the sorted array: " << endl;
	//PrintArray(holding, loops);
	cout << "Did the array contain the target? " << (FindValueInSortedArray(target, holding, loops) ? "true" : "false") << endl;
	cout << "Did the array contain duplicates? " << (FindDuplicatesInSortArray(holding, loops) ? "true" : "false") << endl;

	delete[] holding;
	system("pause");
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
